extends KinematicBody

var motion = Vector3()
export var player_id:String = "1"
var can_move = true

var spawn_location

const SPEED = 10
const UP = Vector3(0, 1, 0)


func _ready():
	spawn_location = get_tree().get_root().find_node("Player%sSpawn" % player_id, true, false)


func _physics_process(delta):
	if not can_move:
		return
	move()
	animate()
	face_forward()


func move():
	var x = Input.get_action_strength("right_%s" % player_id) - Input.get_action_strength("left_%s" % player_id)
	var z = Input.get_action_strength("down_%s" % player_id) - Input.get_action_strength("up_%s" % player_id)
	motion = Vector3(x, 0, z)
	move_and_slide(motion.normalized() * SPEED)


func animate():
	if motion.length() > 0:
		$AnimationPlayer.play("Arms Cross Walk")
	else:
		$AnimationPlayer.stop()


func face_forward():
	if not motion.x == 0 or not motion.z == 0:
		look_at(Vector3(-motion.x, 0, -motion.z) * SPEED, UP)


func freeze(id):
	can_move = false
	if str(id) == player_id:
		$Particles.emitting = true
	

func reset():
	can_move = true
	translation = spawn_location.translation
