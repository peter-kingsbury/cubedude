extends SpotLight

func _ready():
	hide()


func freeze(player):
	var target = get_tree().get_root().find_node("Player%s" % player, true, false)
	look_at(target.translation, Vector3(0, 1, 0))
	show()


func reset():
	hide()
