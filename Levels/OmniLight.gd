extends OmniLight

onready var starting_light_energy = light_energy

func ready():
	reset()

func freeze(id):
	$AnimationPlayer.play("Fade")
	
func reset():
	light_energy = 1.2
