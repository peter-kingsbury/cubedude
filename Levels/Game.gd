extends Spatial


var Player1_score = 0
var Player2_score = 0

export var target_score = 3

func _ready():
	randomize()
	var blocks = $Scenery/Blocks.get_children()
	var min_angle = -360
	var max_angle = 360
	for block in blocks:
		var x = (randf() * (max_angle-min_angle)) + min_angle
		var y = (randf() * (max_angle-min_angle)) + min_angle
		var z = (randf() * (max_angle-min_angle)) + min_angle
		block.rotation_degrees = Vector3(x, y, z)


func _on_GoalDetector_body_entered(_body, goal_id):
	print("Player " + str(goal_id) + " has scored a goal")
	get_tree().call_group("game_pieces", "freeze", goal_id)
	$Timer.start()
	update_score(goal_id)
	$Airhorn.play()


func _on_Timer_timeout():
	get_tree().call_group("game_pieces", "reset")


func update_score(player_id):
	var new_score
	
	match(player_id):
		1:
			Player1_score += 1
			new_score = Player1_score
		2:
			Player2_score += 1
			new_score = Player2_score

	$GUI.update_score(player_id, new_score)
	check_game_over(player_id, new_score)


func check_game_over(player, score):
	if score == target_score:
		$Timer.queue_free()
		$GUI.game_over(player)

